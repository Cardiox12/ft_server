FROM debian:buster

RUN apt-get update
RUN apt-get install -y wget vim  mariadb-server nginx php-cli php-fpm php-mysql openssl 

ENV AUTO_INDEX="on"

RUN mkdir /etc/nginx/ssl
RUN chmod 700 /etc/nginx/ssl
RUN wget https://github.com/FiloSottile/mkcert/releases/download/v1.1.2/mkcert-v1.1.2-linux-amd64 -P /tmp
RUN mv /tmp/mkcert-v1.1.2-linux-amd64 /tmp/mkcert
RUN chmod +x /tmp/mkcert
RUN ./tmp/mkcert -install
RUN ./tmp/mkcert nginx-ssl-cert
RUN mv /nginx-ssl-cert.pem /etc/nginx/ssl/
RUN mv /nginx-ssl-cert-key.pem /etc/nginx/ssl/

# Setting up phpmyadmin
RUN mkdir /var/www/html/phpmyadmin
COPY ./srcs/config.inc.php /var/www/html/phpmyadmin

# Setting up the nginx.conf file
RUN mkdir /confs
COPY ./srcs/nginx.conf_1 /confs
COPY ./srcs/nginx.conf /confs

# Setting up Wordpress
COPY ./srcs/wp-config.sql /tmp
COPY ./srcs/wp-config.php /var/www/html/wordpress/

# Setting up my bonus
RUN mkdir /var/www/html/bonus
COPY ./srcs/bonus /var/www/html/bonus

# Setting up linus torvald tracker
RUN mkdir /var/www/html/linus_torvald
COPY ./srcs/linus_torvald /var/www/html/linus_torvald

# Entrypoint configuration
COPY srcs/startup.sh /usr/local/bin
RUN chmod 777 /usr/local/bin/startup.sh

EXPOSE 80 443
ENTRYPOINT ["startup.sh"]
