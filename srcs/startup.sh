#!/bin/bash

# apt-get install -y php-{mbstring,zip,gd,xml,pear,gettext,cli,fpm,cgi}
service php7.3-fpm start
service mysql start

if [ "$AUTO_INDEX" == "on" ]; then
    mv /confs/nginx.conf /etc/nginx/
else
    mv /confs/nginx.conf_1 /etc/nginx/nginx.conf
fi

# Setting up PhpMyAdmin
wget https://files.phpmyadmin.net/phpMyAdmin/4.9.4/phpMyAdmin-4.9.4-all-languages.tar.gz -P /tmp
tar -xvf /tmp/phpMyAdmin-4.9.4-all-languages.tar.gz --strip-components=1 -C /var/www/html/phpmyadmin
chmod 660 /var/www/html/phpmyadmin/config.inc.php
chown -R www-data:www-data /var/www/

# Setting up wordpress
mkdir /var/www/html/wordpress
wget  https://fr.wordpress.org/latest-fr_FR.tar.gz -P /tmp
mkdir /var/www/html/wordpress
tar -xvf /tmp/latest-fr_FR.tar.gz --strip-components=1 -C /var/www/html/wordpress
mysql -u root < /tmp/wp-config.sql

service nginx start
# nginx -g 'daemon off;'
bash
tail -f /dev/null
