const sounds = Array.from(document.querySelectorAll('audio'));
const linus_head = document.querySelector('.linus-head-container');

function random(min, max){
    if (min >= max)
        throw Error("Min cannot be greater or equal to max.");

    return Math.floor(Math.random() * max) + min; 
}

function pick(array){
    return array[random(0, array.length)];
}

linus_head.addEventListener('click', (e) => {
    let sound = pick(sounds);

    sound.play()
});